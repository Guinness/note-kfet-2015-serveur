#!/bin/bash

# A exécuter en root

if [[ $USER == root ]]
then
 service postgresql restart

 echo ""
 echo "Creating database mails"
 sudo -u postgres psql -f .recreate_database_mails.sql -d postgres

 echo ""
 echo "Creating tables"
 sudo -u note psql nkmails -f .recreate_tables_mails.sql #2>&1 | grep -A 2 "ERREUR" --color

 echo "done"
else
 echo "Doit être exécuté en tant que root. (Et root doit avoir l'accès à la base pgsql en SUPERUSER)."
fi
