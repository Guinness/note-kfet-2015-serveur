#!/usr/bin/python
# -*- coding: utf-8 -*-

from __future__ import unicode_literals, print_function

import sys

import psycopg2, psycopg2.extras
from jinja2 import Environment, PackageLoader

if '/home/note/note-kfet-2015-serveur/' not in sys.path:
    sys.path.append('/home/note/note-kfet-2015-serveur/')
import mail

if __name__ == '__main__':
    # On initialise le template
    env = Environment(loader=PackageLoader('mail', 'templates'))
    template = env.get_template('template_wei_1a')

    # On prépare la connexion à la base de données
    con = psycopg2.connect(database='note', user='note')
    cur = con.cursor(cursor_factory=psycopg2.extras.DictCursor)

    cur.execute("SELECT * FROM wei_1a WHERE NOT triggered;")
    liste_1a = cur.fetchall()

    # Paramètres commun à tous les mails
    subject = "Inscription au High[WEI] To Hell"
    emetteur = "wei@note.crans.org"
    reply_to = ['weiensc@gmail.com',]

    for entry in liste_1a:
        entry["prenom"] = entry["prenom"].decode('utf-8')
        entry["nom"] = entry["nom"].decode('utf-8')

        to = [entry["mail"],]
        body = template.render(**{ 'user' : entry })

        mail.queue_mail(emetteur, to, subject, body, cc=[], replyto=reply_to)

        cur.execute("UPDATE wei_1a SET triggered = TRUE WHERE idwei=%s;", (entry["idwei"],))

        cur.execute("COMMIT;")
