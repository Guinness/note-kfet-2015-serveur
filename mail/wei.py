#!/usr/bin/python
#-*- coding: utf-8 -*-

from __future__ import unicode_literals

import sys
from datetime import datetime, timedelta

from jinja2 import Environment, PackageLoader
import psycopg2, psycopg2.extras

if '/home/note/note-kfet-2015-serveur/mail/' not in sys.path:
    sys.path.append('/home/note/note-kfet-2015-serveur/mail/')
from mail import queue_mail

def human_roles(roles_list):
    """
        Transforme un rôle écrit au format BDD en rôle
        lisible par un être humain
    """
    liste = roles_list.split(';')
    liste_human = []

    for role in liste:
        if role == 'inconnu':
            liste_human.append('Rôle inconnu')
        elif role == 'libre':
            liste_human.append('Électron libre')
        elif role == 'chef_bus':
            liste_human.append('Chef de bus')
        elif role == 'chef_equipe':
            liste_human.append('Chef d\'équipe')
        elif role == 'staff':
            liste_human.append('Staff')

    return ' ou '.join(liste_human)

emetteur = 'wei@note.crans.org'
objet = '[WEI 2015] État de ta préinscription'
reply_to = ['weiensc2015@gmail.com',]

if __name__ == '__main__':
    #  Chargement du template de mail
    env = Environment(loader=PackageLoader('mail', 'templates'))
    env.filters['human_roles'] = human_roles

    template = env.get_template('template_wei')

    # Préparation de la connexion à la base de données
    conn = psycopg2.connect(database='note', user='note')
    cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

    # Récupération des inscriptions dans la base
    cur.execute("SELECT payé as paye,* FROM wei_vieux;")

    liste_inscriptions = cur.fetchall()

    # On traite les inscriptions
    for inscription in liste_inscriptions:

        paye = inscription['paye']
        caution = inscription['caution']
        preciser_bus = 'Je ne sais pas' == inscription['bus'].decode('utf-8')
        preciser_role = ';' in inscription['role']

        if paye and caution and not (preciser_bus or preciser_role):
            # Rien à redire -> on passe à la suite
            continue

        else:
            cur.execute("SELECT * FROM comptes WHERE idbde=%s;", (inscription['idbde'],))
            user = cur.fetchone()
            prix_wei = 16500 if user["normalien"] else 9500
            peut_payer = user["solde"] >= prix_wei

            contexte = {
                'inscription' : inscription,
                'user' : user,
                'paye' : paye,
                'peut_payer' : peut_payer,
                'caution' : caution,
                'preciser_bus' : preciser_bus,
                'preciser_role' : preciser_role,
            }

            body = template.render(**contexte)

            queue_mail(emetteur, [inscription["mail"],], objet, body, cc=[], replyto=reply_to)
