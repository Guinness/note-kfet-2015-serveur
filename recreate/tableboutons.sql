--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: boutons; Type: TABLE; Schema: public; Owner: note; Tablespace: 
--
--DROP TABLE boutons;
--
--CREATE TABLE boutons (
--    id integer NOT NULL,
--    label character varying DEFAULT ''::character varying,
--    montant integer DEFAULT 0,
--    destinataire integer DEFAULT (-10),
--    categorie character varying DEFAULT ''::character varying,
--    affiche boolean DEFAULT true,
--    CONSTRAINT categorie_doit_etre_specifiee CHECK (((categorie)::text <> ''::text)),
--    CONSTRAINT label_doit_etre_specifie CHECK (((label)::text <> ''::text))
--);


ALTER TABLE public.boutons OWNER TO note;

--
-- Name: boutons_id_seq; Type: SEQUENCE; Schema: public; Owner: note
--

--CREATE SEQUENCE boutons_id_seq
--    START WITH 1
--    INCREMENT BY 1
--    NO MINVALUE
--    NO MAXVALUE
--    CACHE 1;


ALTER TABLE public.boutons_id_seq OWNER TO note;

--
-- Name: boutons_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: note
--

ALTER SEQUENCE boutons_id_seq OWNED BY boutons.id;


--
-- Name: boutons_id_seq; Type: SEQUENCE SET; Schema: public; Owner: note
--

SELECT pg_catalog.setval('boutons_id_seq', 103, true);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: note
--

ALTER TABLE boutons ALTER COLUMN id SET DEFAULT nextval('boutons_id_seq'::regclass);


--
-- Data for Name: boutons; Type: TABLE DATA; Schema: public; Owner: note
--

COPY boutons (id, label, montant, destinataire, categorie, affiche) FROM stdin;
1	Coca	90	0	Soft	t
2	Burger	150	0	Bouffe	t
4	Frites	1	0	Bouffe	t
16	Limo Pinte	60	0	Soft	t
17	Limo Demi	40	0	Soft	t
18	Café	40	0	Soft	t
19	Chocolat	40	0	Soft	t
20	Lait	50	0	Soft	t
21	Viennoiserie	90	0	Bouffe	t
22	PtitDej	250	0	Bouffe	t
23	Sand 1/2	250	0	Bouffe	t
24	Barres Choco	60	0	Bouffe	t
25	Dragibus	30	0	Bouffe	t
27	Jus ptit dej	40	0	Soft	t
28	Skittles	60	0	Bouffe	t
30	Photocopie	5	0	Autre	t
31	Leffe Rad	220	0	Alcool	t
32	Delirium	200	0	Alcool	t
33	Adelscott	200	0	Alcool	t
34	BurgerFrite	250	0	Bouffe	t
35	Pampryl	70	0	Soft	t
36	Thé	50	0	Soft	t
37	Carlsberg	210	0	Alcool	t
38	Guinness	270	0	Alcool	t
39	Chimay Bleue	200	0	Alcool	t
40	Compote	30	0	Bouffe	t
41	Despérados	200	0	Alcool	t
42	Canette soda	80	0	Soft	t
43	Demi	130	0	Alcool	t
44	Pinte	200	0	Alcool	t
45	plat seul	250	0	Bouffe	t
46	Lays	60	0	Bouffe	t
48	Leffe Tr	210	0	Alcool	t
49	Bonne humeur =)	0	0	Autre	t
50	Demi Baguette	50	0	Bouffe	t
51	Fût 50L	12000	0	Alcool	t
52	Pringles 165g	220	0	Bouffe	t
53	Corona	200	0	Alcool	t
54	Fosters	140	0	Alcool	t
55	Faro	130	0	Alcool	t
56	Grimbergen	170	0	Alcool	t
57	Maltesers maxi	170	0	Bouffe	t
58	GrosDej	400	0	Bouffe	t
59	Hoegaarden	150	0	Alcool	t
60	Kasteel	230	0	Alcool	t
61	Leffe	140	0	Alcool	t
62	lasagnes	150	0	Bouffe	t
63	Smirnoff	190	0	Alcool	t
64	Burger*2/Frite*1	400	0	Bouffe	t
65	Photocopies x 10	50	0	Autre	t
66	Carambar	15	0	Bouffe	t
67	Chupa Chups	20	0	Bouffe	t
68	Kwak	170	0	Alcool	t
69	Pelforth	150	0	Alcool	t
70	Pinte Grim	250	0	Alcool	t
71	Demi Grim	160	0	Alcool	t
72	Chimay Rouge	180	0	Alcool	t
73	Krok Saumon	150	0	Bouffe	t
74	Triple K Karmeliet	200	0	Alcool	t
75	pates	200	0	Bouffe	t
76	Verre coca	50	0	Soft	t
77	Monaco Pinte	160	0	Alcool	t
78	sangria	100	0	Alcool	t
79	Demi 1664	160	0	Alcool	t
80	Pinte 1664	250	0	Alcool	t
81	 carlsberg XL	320	0	Alcool	t
82	sirop	0	0	Soft	t
83	Pop corn	150	0	Bouffe	t
84	Menu 3	300	0	Bouffe	t
85	tombola	100	0	Autre	t
86	pringles 40g	110	0	Bouffe	t
87	Pancake	50	0	Bouffe	t
88	Repas de noël	1500	0	Autre	t
89	Fruits	40	0	Bouffe	t
91	Repas	200	0	Bouffe	t
92	Plat	150	0	Bouffe	t
93	Dessert	50	0	Bouffe	t
94	Panache pinte	140	0	Alcool	t
95	Bière Pong	300	0	Alcool	t
96	sweat ENS Z	3500	0	Autre	t
97	sweat ENS NZ	3000	0	Autre	t
98	carte BDE	4000	0	Autre	t
99	Krok	100	0	Bouffe	t
100	Pecheresse	160	0	Alcool	t
101	Salade	250	0	Bouffe	t
102	Demi Kokarde	130	0	Alcool	t
103	Pinte Kokarde	200	0	Alcool	t
3	Useless	0	0	Autre	f
\.


--
-- Name: id_boutons_est_un_id; Type: CONSTRAINT; Schema: public; Owner: note; Tablespace: 
--

--ALTER TABLE ONLY boutons
--    ADD CONSTRAINT id_boutons_est_un_id PRIMARY KEY (id);


--
-- Name: pas_deux_fois_le_meme_bouton; Type: CONSTRAINT; Schema: public; Owner: note; Tablespace: 
--

--ALTER TABLE ONLY boutons
--    ADD CONSTRAINT pas_deux_fois_le_meme_bouton UNIQUE (label, categorie);


--
-- Name: destinataire_doit_exister; Type: FK CONSTRAINT; Schema: public; Owner: note
--

--ALTER TABLE ONLY boutons
--    ADD CONSTRAINT destinataire_doit_exister FOREIGN KEY (destinataire) REFERENCES comptes(idbde);


--
-- PostgreSQL database dump complete
--

