#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
import sys
import psycopg2
import psycopg2.extras


db_nk12 = "bde"
db_nk15 = "note"

DEBUG = False;

def getcursor(db):
    """Renvoie une connexion à la base demandé et un curseur"""
    con = psycopg2.connect(database=db)
    cur = con.cursor(cursor_factory=psycopg2.extras.DictCursor)
    return con, cur

if __name__ == "__main__":
    if "-v" in sys.argv or "--debug" in sys.argv or "--verbose" in sys.argv:
        DEBUG = True
    if "--rig" in sys.argv:
		import_adherents()
