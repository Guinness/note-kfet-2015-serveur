UPDATE cheques 
SET idbde=transactions.emetteur 
FROM transactions 
WHERE idtransaction=transactions.id 
AND retrait;

UPDATE cheques 
SET idbde=transactions.destinataire 
FROM transactions 
WHERE idtransaction=transactions.id 
AND NOT retrait;

UPDATE cb 
SET idbde=transactions.emetteur 
FROM transactions 
WHERE idtransaction=transactions.id 
AND retrait;

UPDATE cb 
SET idbde=transactions.destinataire 
FROM transactions 
WHERE idtransaction=transactions.id 
AND NOT retrait;

UPDATE virements 
SET idbde=transactions.emetteur 
FROM transactions 
WHERE idtransaction=transactions.id 
AND retrait;

UPDATE virements
SET idbde=transactions.destinataire 
FROM transactions 
WHERE idtransaction=transactions.id 
AND NOT retrait;
