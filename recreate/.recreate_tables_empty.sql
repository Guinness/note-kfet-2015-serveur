--- A exécuter sur la base note
--- recrée toutes les tables comme elles l'ont etées la première fois

--- Apparemment le langage plpgsql existe déjà
--- CREATE LANGUAGE plpgsql;

CREATE TABLE comptes
(idbde SERIAL CONSTRAINT id_bde_est_un_id PRIMARY KEY,
type varchar DEFAULT 'personne' CHECK (type in ('personne','club','special')),
pseudo varchar DEFAULT '' CONSTRAINT pseudo_unique UNIQUE, CONSTRAINT pseudo_non_vide CHECK (pseudo!=''),
passwd varchar DEFAULT '*|*' CHECK (passwd LIKE '%|%'),
solde int DEFAULT 0,
nom varchar CONSTRAINT nom_non_vide NOT NULL,
prenom varchar CONSTRAINT prenom_non_vide NOT NULL,
tel varchar DEFAULT '',
mail varchar DEFAULT '' CHECK (mail LIKE '%@%.%'),
adresse varchar DEFAULT '',
sexe char (1) DEFAULT 'M',
fonction varchar DEFAULT '',
normalien bool DEFAULT false,
pbsante varchar DEFAULT '',
droits varchar DEFAULT 'basic',
surdroits varchar DEFAULT '',
supreme bool DEFAULT false,
report_period int DEFAULT (-1),
previous_report_date timestamp DEFAULT NOW(),
next_report_date timestamp DEFAULT '01/01/2042 00:13:37',
bloque bool DEFAULT false,
last_adhesion int DEFAULT -1,
commentaire varchar DEFAULT '',
last_negatif timestamp DEFAULT NULL,
deleted bool DEFAULT false);

CREATE INDEX index_idbde ON comptes (idbde);  --- permettra de stocker la table dans l'ordre

CREATE FUNCTION keep_max_id() RETURNS trigger AS $comptes_keep_id_max$
BEGIN
PERFORM setval('comptes_idbde_seq',(SELECT max(idbde) FROM comptes));
RETURN NEW;
END;
$comptes_keep_id_max$ LANGUAGE plpgsql;

CREATE TRIGGER keep_max_id BEFORE INSERT ON comptes EXECUTE PROCEDURE keep_max_id();

CREATE TABLE preinscriptions
(preid SERIAL PRIMARY KEY,
type varchar DEFAULT 'personne' CHECK (type='personne' OR type='club'),
nom varchar CONSTRAINT nom_non_vide NOT NULL,
prenom varchar CONSTRAINT prenom_non_vide NOT NULL,
tel varchar DEFAULT '',
mail varchar DEFAULT '' CHECK (mail LIKE '%@%'),
adresse varchar DEFAULT '',
sexe char (1) DEFAULT 'M',
normalien bool DEFAULT false,
pbsante varchar DEFAULT '',
section varchar DEFAULT '');

CREATE TABLE adhesions
(id SERIAL PRIMARY KEY,
idbde int DEFAULT 0 CONSTRAINT idbde_doit_etre_specifie CHECK (idbde!=0),
annee int DEFAULT 0 CONSTRAINT annee_doit_etre_specifiee CHECK (annee!=0),
wei bool DEFAULT false,
idtransaction int DEFAULT 0 CONSTRAINT une_transaction_par_adhesion UNIQUE,CONSTRAINT idtransaction_doit_etre_specifie CHECK (idtransaction!=0),
date timestamp DEFAULT NOW(),
section varchar DEFAULT '' CONSTRAINT section_doit_etre_specifiee CHECK (section!=''),
FOREIGN KEY idbde REFERENCES comptes (idbde),
FOREIGN KEY idtransaction REFERENCES transactions (id)
);

CREATE TABLE transactions
(id SERIAL PRIMARY KEY,
date timestamp DEFAULT clock_timestamp(),
type varchar DEFAULT '' CONSTRAINT type_transaction_doit_etre_specifie CHECK (type!=''),
emetteur int DEFAULT -99 CONSTRAINT emetteur_doit_etre_specifie CHECK (emetteur!=(-99)),
destinataire int DEFAULT -99 CONSTRAINT destinataire_doit_etre_specifie CHECK (destinataire!=(-99)),
quantite int DEFAULT 1,
montant int DEFAULT 0,
description varchar DEFAULT '',
valide bool DEFAULT true,
cantinvalidate bool DEFAULT false,
FOREIGN KEY emetteur REFERENCES comptes (idbde),
FOREIGN KEY destinataire REFERENCES comptes (idbde)
);

CREATE INDEX transactions_id_idx ON transactions (id);
-- CREATE INDEX transactions_destinataire_idx ON transactions (destinataire);
-- CREATE INDEX transactions_emetteur_idx ON transactions (emetteur);
CREATE INDEX transactions_date_idx ON transactions (date);

CREATE FUNCTION no_delete_transaction() RETURNS trigger AS $transactions_no_del_transaction$
BEGIN
RAISE EXCEPTION 'Ne peut pas supprimer de transaction.';
END;
$transactions_no_del_transaction$ LANGUAGE plpgsql;

CREATE TRIGGER no_delete_transaction BEFORE DELETE ON transactions EXECUTE PROCEDURE no_delete_transaction();

CREATE TABLE log
(id SERIAL PRIMARY KEY,
date timestamp DEFAULT now(),
ip varchar DEFAULT '' CONSTRAINT ip_doit_etre_specifiee CHECK (ip != ''),
utilisateur varchar DEFAULT '' CONSTRAINT utilisateur_doit_etre_specifie CHECK (utilisateur != ''),
fonction varchar DEFAULT '' CONSTRAINT something_has_to_be_done CHECK(fonction != ''),
params varchar DEFAULT '');

CREATE TABLE cheques
(id SERIAL PRIMARY KEY,
date timestamp DEFAULT now(),
nom varchar DEFAULT '' CONSTRAINT nom_doit_etre_specifie CHECK (nom!=''),
prenom varchar DEFAULT '' CONSTRAINT prenom_doit_etre_specifie CHECK (prenom!=''),
banque varchar DEFAULT '',
idbde int DEFAULT 0 CONSTRAINT idbde_doit_etre_specifie CHECK (idbde!=0),
idtransaction int DEFAULT 0 CONSTRAINT idtransaction_doit_etre_specifie CHECK (idtransaction!=0),
retrait bool DEFAULT false,
checked bool DEFAULT false,
FOREIGN KEY idbde REFERENCES comptes (idbde),
FOREIGN KEY idtransaction REFERENCES transactions (id)
);

CREATE TABLE virements
(id SERIAL PRIMARY KEY,
date timestamp DEFAULT now(),
nom varchar DEFAULT '' CONSTRAINT nom_doit_etre_specifie CHECK (nom!=''),
prenom varchar DEFAULT '' CONSTRAINT prenom_doit_etre_specifie CHECK (prenom!=''),
banque varchar DEFAULT '',
idbde int DEFAULT 0 CONSTRAINT idbde_doit_etre_specifie CHECK (idbde!=0),
idtransaction int DEFAULT 0 CONSTRAINT idtransaction_doit_etre_specifie CHECK (idtransaction!=0),
retrait bool DEFAULT false,
checked bool DEFAULT false,
FOREIGN KEY idbde REFERENCES comptes (idbde),
FOREIGN KEY idtransaction REFERENCES transactions (id)
);

CREATE TABLE carte_bancaires
(id SERIAL PRIMARY KEY,
date timestamp DEFAULT now(),
nom varchar DEFAULT '' CONSTRAINT nom_doit_etre_specifie CHECK (nom!=''),
prenom varchar DEFAULT '' CONSTRAINT prenom_doit_etre_specifie CHECK (prenom!=''),
banque varchar DEFAULT '',
idbde int DEFAULT 0 CONSTRAINT idbde_doit_etre_specifie CHECK (idbde!=0),
idtransaction int DEFAULT 0 CONSTRAINT idtransaction_doit_etre_specifie CHECK (idtransaction!=0),
retrait bool DEFAULT false,
checked bool DEFAULT false,
FOREIGN KEY idbde REFERENCES comptes (idbde),
FOREIGN KEY idtransaction REFERENCES transactions (id)
);

CREATE TABLE configurations
(id SERIAL PRIMARY KEY,
used bool DEFAULT 'f',
nom varchar DEFAULT '' CONSTRAINT nom_configuration_doit_etre_specifie CHECK (nom!=''),
prix_wei_normalien int DEFAULT NULL CONSTRAINT config_totale1 CHECK (NOT prix_wei_normalien is NULL),
prix_wei_non_normalien int DEFAULT NULL CONSTRAINT config_totale2 CHECK (NOT prix_wei_non_normalien is NULL),
prix_adhesion int DEFAULT NULL CONSTRAINT config_totale3 CHECK (NOT prix_adhesion is NULL),
solde_negatif int DEFAULT NULL CONSTRAINT config_totale4 CHECK (NOT solde_negatif is NULL),
solde_tres_negatif int DEFAULT NULL CONSTRAINT config_totale5 CHECK (NOT solde_tres_negatif is NULL),
solde_pas_plus_negatif int DEFAULT NULL CONSTRAINT config_totale6 CHECK (NOT solde_pas_plus_negatif is NULL),
start_next_year_month int DEFAULT NULL CONSTRAINT config_totale7 CHECK (NOT start_next_year_month is NULL),
start_next_year_day int DEFAULT NULL CONSTRAINT config_totale8 CHECK (NOT start_next_year_day is NULL),
end_previous_year_month int DEFAULT NULL CONSTRAINT config_totale9 CHECK (NOT end_previous_year_month is NULL),
end_previous_year_day int DEFAULT NULL CONSTRAINT config_totale10 CHECK (NOT end_previous_year_day is NULL),
year_boot_month int DEFAULT NULL CONSTRAINT config_totale11 CHECK (NOT year_boot_month is NULL),
year_boot_day int DEFAULT NULL CONSTRAINT config_totale12 CHECK (NOT year_boot_day is NULL),
historique_pseudo_timeout int DEFAULT NULL CONSTRAINT config_totale13 CHECK (NOT historique_pseudo_timeout is NULL),
historique_incompressible int DEFAULT NULL CONSTRAINT config_totale14 CHECK (NOT historique_incompressible is NULL),
liste_invites_opening_time int DEFAULT NULL CONSTRAINT config_totale15 CHECK (NOT liste_invites_opening_time is NULL),
liste_invites_closing_time int DEFAULT NULL CONSTRAINT config_totale16 CHECK (NOT liste_invites_closing_time is NULL),
max_invitation_par_personne int DEFAULT NULL CONSTRAINT config_totale17 CHECK (NOT max_invitation_par_personne is NULL),
max_invitation_par_an int DEFAULT NULL CONSTRAINT config_totale18 CHECK (NOT max_invitation_par_an is NULL),
toggle_transaction_timeout int DEFAULT NULL CONSTRAINT config_totale19 CHECK (NOT toggle_transaction_timeout is NULL),
token_regenerate_password_delay int DEFAULT NULL CONSTRAINT config_totale20 CHECK (NOT token_regenerate_password_delay is NULL),
solde_mail_passage_negatif int DEFAULT NULL CONSTRAINT config_totale21 CHECK (NOT solde_mail_passage_negatif is NULL)
);

CREATE TABLE boutons
(id SERIAL PRIMARY KEY,
label varchar DEFAULT '' CONSTRAINT label_doit_etre_specifie CHECK (label!=''),
montant int DEFAULT 0,
destinataire int DEFAULT -10, CONSTRAINT destinataire_doit_exister FOREIGN KEY (destinataire) REFERENCES comptes (idbde),
categorie varchar DEFAULT '' CONSTRAINT categorie_doit_etre_specifiee CHECK (categorie!=''),
affiche bool DEFAULT true,
description varchar DEFAULT '',
consigne bool DEFAULT false,
CONSTRAINT pas_deux_fois_le_meme_bouton UNIQUE (label,categorie));

CREATE TABLE aliases
(id SERIAL PRIMARY KEY,
alias varchar DEFAULT '' CONSTRAINT alias_vide_interdit CHECK (alias != ''),
idbde int DEFAULT -100 CONSTRAINT idbde_doit_etre_specifie CHECK (idbde!=(-100)), UNIQUE (alias),
FOREIGN KEY idbde REFERENCES comptes (idbde)
);

CREATE TABLE historique
(id SERIAL PRIMARY KEY,
idbde int DEFAULT -100 CONSTRAINT idbde_doit_etre_specifie CHECK (idbde!=(-100)),
avant varchar DEFAULT '' CONSTRAINT pseudo_avant_vide_impossible CHECK (avant != ''),
apres varchar DEFAULT '' CONSTRAINT pseudo_apres_vide_impossible CHECK (apres != ''),
date timestamp DEFAULT NULL CONSTRAINT date_doit_etre_specifiee CHECK (NOT date is NULL),
valide bool DEFAULT false,
FOREIGN KEY idbde REFERENCES comptes (idbde)
);

CREATE TABLE activites
(id SERIAL PRIMARY KEY,
 debut timestamp NOT NULL,
 fin timestamp NOT NULL CONSTRAINT activite_a_une_duree_positive CHECK (debut<=fin),
 titre varchar DEFAULT '' CONSTRAINT titre_doit_etre_specifie CHECK (titre!=''),
 lieu varchar DEFAULT '',
 description varchar DEFAULT '',
 responsable int DEFAULT 0 CONSTRAINT responsable_doit_etre_specifie CHECK (responsable!=0),
 signature varchar DEFAULT '',
 validepar int DEFAULT -100,
 liste bool DEFAULT false,
 listeimprimee bool DEFAULT false,
FOREIGN KEY responsable REFERENCES comptes (idbde),
FOREIGN KEY validepar REFERENCES comptes (idbde)
);

CREATE TABLE invites
(id SERIAL PRIMARY KEY,
 nom varchar DEFAULT '' CONSTRAINT nom_doit_etre_specifie CHECK (nom!=''),
 prenom varchar DEFAULT '' CONSTRAINT prenom_doit_etre_specifie CHECK (prenom!=''),
 responsable int DEFAULT (-1) CONSTRAINT responsable_doit_etre_specifie CHECK (responsable!=(-1)),
 activite int DEFAULT (-1) CONSTRAINT activite_doit_etre_specifie CHECK (activite!=(-1)),
FOREIGN KEY responsable REFERENCES comptes (idbde),
FOREIGN KEY activite REFERENCES activites (id)
 );
 
CREATE OR REPLACE FUNCTION start_end_year (annee int DEFAULT 0, OUT debut date,OUT fin date) RETURNS record
--- Renvoie le début et la fin de l'annee ``annee`` pour savoir sur quelle période quelqu'un ayant
--- adhéré peut utiliser son compte.
AS $BODY$
DECLARE snym int;
        snyd int;
        epym int;
        epyd int;
        ybm int;
        ybd int;
BEGIN
  SELECT start_next_year_month,start_next_year_day,end_previous_year_month,end_previous_year_day,year_boot_month,year_boot_day
         INTO snym,snyd,epym,epyd,ybm,ybd
         FROM configurations
         WHERE used='t';
  IF (annee=0)
  THEN
      annee:=EXTRACT(year FROM current_date);
      IF (current_date<CAST(annee||'-'||ybm||'-'||ybd AS date))  ---si on est avant le year boot, c'est qu'on est encore l'année dernière
      THEN
          annee:=annee-1;
      END IF;
  END IF;
  debut:=CAST(annee||'-'||snym||'-'||snyd AS date);
  fin:=CAST((annee+1)||'-'||epym||'-'||epyd AS date);
END
$BODY$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION previous_year_boot (date date DEFAULT NULL, OUT boot date) RETURNS date
--- Renvoie le dernier year_boot précédant ``date``. (Si NULL, on prend NOW()).
AS $BODY$
DECLARE ybm int;
        ybd int;
BEGIN
  SELECT year_boot_month,year_boot_day
         INTO ybm,ybd
         FROM configurations
         WHERE used='t';
  IF (date is NULL)
  THEN
      date:=NOW();
  END IF;
  boot:=CAST(EXTRACT(year FROM date) || '-' || ybm || '-' || ybd AS date);
  IF (boot > date)
  THEN
      --- Si on est entre janvier et ybm, le year_boot calculé est dans le futur, or on veut le précédent
      boot:=CAST(EXTRACT(year FROM date)-1 || '-' || ybm || '-' || ybd AS date);
  END IF;
END
$BODY$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION isAdherent (idbdeask int, date date DEFAULT now(), OUT answer bool, OUT section varchar, OUT section_year int)
RETURNS record
AS $BODY$
DECLARE debut date;
        fin date;
        annee int;
        typ varchar;
BEGIN
  SELECT type INTO typ FROM comptes WHERE idbde=idbdeask;
  IF typ='special' THEN
    answer:='t';
    section:='Special';
    RETURN;
  END IF;
  IF typ='club' THEN
    answer:='t';
    section:='Club';
    RETURN;
  END IF;
  SELECT adhesions.annee,adhesions.section INTO annee, section
         FROM adhesions
         WHERE id=(SELECT last_adhesion FROM comptes WHERE idbde=idbdeask);
  IF annee is NULL THEN
    answer:='f';
    section:='Unknown';
    section_year:=-1;
    RETURN;
  END IF;
  section_year:=annee;
  SELECT foo.debut,foo.fin INTO debut,fin FROM start_end_year(annee) AS foo;
  IF (debut<=date) AND (date<=fin) THEN
    answer:='t';
  ELSE
    answer:='f';
  END IF;
END
$BODY$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION section (idbde int DEFAULT -100) RETURNS varchar
AS $BODY$
BEGIN
  IF (idbde=(-100))
  THEN
      RETURN 'Unknown';
  ELSE
      RETURN (SELECT section FROM isAdherent(idbde));
  END IF;
END
$BODY$ LANGUAGE plpgsql;


CREATE TABLE regen_password
(id SERIAL PRIMARY KEY,
 idbde int,
 mail varchar DEFAULT '' CHECK (mail LIKE '%@%.%'),
 token varchar DEFAULT '' CONSTRAINT token_doit_etre_specifie CHECK (token!=''),
 timestamp timestamp DEFAULT now()
 );






--- Table configurations
--- on enregistre la configuration par défaut (qui est used, obviously)
INSERT INTO configurations (used,nom,
prix_wei_normalien, prix_wei_non_normalien, prix_adhesion,
solde_negatif, solde_tres_negatif, solde_pas_plus_negatif,
start_next_year_month, start_next_year_day,
end_previous_year_month, end_previous_year_day,
year_boot_month, year_boot_day,
historique_pseudo_timeout, historique_incompressible,
liste_invites_opening_time, liste_invites_closing_time, max_invitation_par_personne, max_invitation_par_an,
toggle_transaction_timeout,
token_regenerate_password_delay,
solde_mail_passage_negatif)
VALUES ('t','default',
15000, 13500, 4000,
0, -2000, -20000,
08, 01,
10, 01,
09, 01,
365, 24,
672, 1, 3, 5,
604800,
259200,
0);



