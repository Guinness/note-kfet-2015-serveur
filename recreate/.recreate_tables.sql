--- A exécuter sur la base note
--- recrée toutes les tables comme elles l'ont etées la première fois

--- Apparemment le langage plpgsql existe déjà
--- CREATE LANGUAGE plpgsql;

CREATE TABLE comptes
(idbde SERIAL CONSTRAINT id_bde_est_un_id PRIMARY KEY,
type varchar DEFAULT 'personne' CHECK (type in ('personne','club','special')),
pseudo varchar DEFAULT '' CONSTRAINT pseudo_unique UNIQUE, CONSTRAINT pseudo_non_vide CHECK (pseudo!=''),
passwd varchar DEFAULT '*',
solde int DEFAULT 0,
nom varchar CONSTRAINT nom_non_vide NOT NULL,
prenom varchar CONSTRAINT prenom_non_vide NOT NULL,
tel varchar DEFAULT '',
mail varchar DEFAULT '' CHECK (mail LIKE '%@%.%'),
adresse varchar DEFAULT '',
sexe char (1) DEFAULT 'M',
fonction varchar DEFAULT '',
normalien bool DEFAULT false,
pbsante varchar DEFAULT '',
numsecu varchar DEFAULT '',
idcrans int DEFAULT -1,
droits varchar DEFAULT 'login,basic',
surdroits varchar DEFAULT '',
supreme bool DEFAULT false,
report_period int DEFAULT (-1),
previous_report_date timestamp DEFAULT NOW(),
next_report_date timestamp DEFAULT '01/01/2042 00:13:37',
bloque bool DEFAULT false,
last_adhesion int DEFAULT -1,
commentaire varchar DEFAULT '',
deleted bool DEFAULT false);

CREATE INDEX index_idbde ON comptes (idbde);  --- permettra de stocker la table dans l'ordre

CREATE FUNCTION keep_max_id() RETURNS trigger AS $comptes_keep_id_max$
BEGIN
PERFORM setval('comptes_idbde_seq',(SELECT max(idbde) FROM comptes));
RETURN NEW;
END;
$comptes_keep_id_max$ LANGUAGE plpgsql;

CREATE TRIGGER keep_max_id BEFORE INSERT ON comptes EXECUTE PROCEDURE keep_max_id();

CREATE TABLE preinscriptions
(preid SERIAL PRIMARY KEY,
type varchar DEFAULT 'personne' CHECK (type='personne' OR type='club'),
nom varchar CONSTRAINT nom_non_vide NOT NULL,
prenom varchar CONSTRAINT prenom_non_vide NOT NULL,
tel varchar DEFAULT '',
mail varchar DEFAULT '' CHECK (mail LIKE '%@%'),
adresse varchar DEFAULT '',
sexe char (1) DEFAULT 'M',
normalien bool DEFAULT false,
pbsante varchar DEFAULT '',
numsecu varchar DEFAULT '',
idcrans int DEFAULT -1,
section varchar DEFAULT '');

CREATE TABLE adhesions
(id SERIAL PRIMARY KEY,
idbde int DEFAULT 0 CONSTRAINT idbde_doit_etre_specifie CHECK (idbde!=0),
annee int DEFAULT 0 CONSTRAINT annee_doit_etre_specifiee CHECK (annee!=0),
wei bool DEFAULT false,
idtransaction int DEFAULT 0 CONSTRAINT une_transaction_par_adhesion UNIQUE,CONSTRAINT idtransaction_doit_etre_specifie CHECK (idtransaction!=0),
date timestamp DEFAULT NOW(),
section varchar DEFAULT '' CONSTRAINT section_doit_etre_specifiee CHECK (section!=''));

CREATE TABLE transactions
(id SERIAL PRIMARY KEY,
date timestamp DEFAULT clock_timestamp(),
type varchar DEFAULT '' CONSTRAINT type_transaction_doit_etre_specifie CHECK (type!=''),
emetteur int DEFAULT -99 CONSTRAINT emetteur_doit_etre_specifie CHECK (emetteur!=(-99)),
destinataire int DEFAULT -99 CONSTRAINT destinataire_doit_etre_specifie CHECK (destinataire!=(-99)),
quantite int DEFAULT 1,
montant int DEFAULT 0,
description varchar DEFAULT '',
valide bool DEFAULT true,
cantinvalidate bool DEFAULT false);

CREATE FUNCTION no_delete_transaction() RETURNS trigger AS $transactions_no_del_transaction$
BEGIN
RAISE EXCEPTION 'Ne peut pas supprimer de transaction.';
END;
$transactions_no_del_transaction$ LANGUAGE plpgsql;

CREATE TRIGGER no_delete_transaction BEFORE DELETE ON transactions EXECUTE PROCEDURE no_delete_transaction();

CREATE TABLE log
(id SERIAL PRIMARY KEY,
date timestamp DEFAULT now(),
ip varchar DEFAULT '' CONSTRAINT ip_doit_etre_specifiee CHECK (ip != ''),
utilisateur varchar DEFAULT '' CONSTRAINT utilisateur_doit_etre_specifie CHECK (utilisateur != ''),
fonction varchar DEFAULT '' CONSTRAINT something_has_to_be_done CHECK(fonction != ''),
params varchar DEFAULT '');

CREATE TABLE cheques
(id SERIAL PRIMARY KEY,
date timestamp DEFAULT now(),
nom varchar DEFAULT '' CONSTRAINT nom_doit_etre_specifie CHECK (nom!=''),
prenom varchar DEFAULT '' CONSTRAINT prenom_doit_etre_specifie CHECK (prenom!=''),
banque varchar DEFAULT '',
montant int DEFAULT 0,
idtransaction int DEFAULT 0 CONSTRAINT idtransaction_doit_etre_specifie CHECK (idtransaction!=0),
retrait bool DEFAULT false,
checked bool DEFAULT false,
deleted bool DEFAULT false);

CREATE TABLE virements
(id SERIAL PRIMARY KEY,
date timestamp DEFAULT now(),
nom varchar DEFAULT '' CONSTRAINT nom_doit_etre_specifie CHECK (nom!=''),
prenom varchar DEFAULT '' CONSTRAINT prenom_doit_etre_specifie CHECK (prenom!=''),
banque varchar DEFAULT '',
montant int DEFAULT 0,
idtransaction int DEFAULT 0 CONSTRAINT idtransaction_doit_etre_specifie CHECK (idtransaction!=0),
retrait bool DEFAULT false,
checked bool DEFAULT false,
deleted bool DEFAULT false);

CREATE TABLE configurations
(id SERIAL PRIMARY KEY,
used bool DEFAULT 'f',
nom varchar DEFAULT '' CONSTRAINT nom_configuration_doit_etre_specifie CHECK (nom!=''),
prix_wei_normalien int DEFAULT NULL CONSTRAINT config_totale1 CHECK (NOT prix_wei_normalien is NULL),
prix_wei_non_normalien int DEFAULT NULL CONSTRAINT config_totale2 CHECK (NOT prix_wei_non_normalien is NULL),
prix_adhesion int DEFAULT NULL CONSTRAINT config_totale3 CHECK (NOT prix_adhesion is NULL),
solde_negatif int DEFAULT NULL CONSTRAINT config_totale4 CHECK (NOT solde_negatif is NULL),
solde_tres_negatif int DEFAULT NULL CONSTRAINT config_totale5 CHECK (NOT solde_tres_negatif is NULL),
solde_pas_plus_negatif int DEFAULT NULL CONSTRAINT config_totale6 CHECK (NOT solde_pas_plus_negatif is NULL),
start_next_year_month int DEFAULT NULL CONSTRAINT config_totale7 CHECK (NOT start_next_year_month is NULL),
start_next_year_day int DEFAULT NULL CONSTRAINT config_totale8 CHECK (NOT start_next_year_day is NULL),
end_previous_year_month int DEFAULT NULL CONSTRAINT config_totale9 CHECK (NOT end_previous_year_month is NULL),
end_previous_year_day int DEFAULT NULL CONSTRAINT config_totale10 CHECK (NOT end_previous_year_day is NULL),
year_boot_month int DEFAULT NULL CONSTRAINT config_totale11 CHECK (NOT year_boot_month is NULL),
year_boot_day int DEFAULT NULL CONSTRAINT config_totale12 CHECK (NOT year_boot_day is NULL),
historique_pseudo_timeout int DEFAULT NULL CONSTRAINT config_totale13 CHECK (NOT historique_pseudo_timeout is NULL),
historique_incompressible int DEFAULT NULL CONSTRAINT config_totale14 CHECK (NOT historique_incompressible is NULL),
liste_invites_opening_time int DEFAULT NULL CONSTRAINT config_totale15 CHECK (NOT liste_invites_opening_time is NULL),
liste_invites_closing_time int DEFAULT NULL CONSTRAINT config_totale16 CHECK (NOT liste_invites_closing_time is NULL),
max_invitation_par_personne int DEFAULT NULL CONSTRAINT config_totale17 CHECK (NOT max_invitation_par_personne is NULL),
max_invitation_par_an int DEFAULT NULL CONSTRAINT config_totale18 CHECK (NOT max_invitation_par_an is NULL),
toggle_transaction_timeout int DEFAULT NULL CONSTRAINT config_totale19 CHECK (NOT toggle_transaction_timeout is NULL)
);

CREATE TABLE boutons
(id SERIAL PRIMARY KEY,
label varchar DEFAULT '' CONSTRAINT label_doit_etre_specifie CHECK (label!=''),
montant int DEFAULT 0,
destinataire int DEFAULT -10, CONSTRAINT destinataire_doit_exister FOREIGN KEY (destinataire) REFERENCES comptes (idbde),
categorie varchar DEFAULT '' CONSTRAINT categorie_doit_etre_specifiee CHECK (categorie!=''),
affiche bool DEFAULT true,
description varchar DEFAULT '',
consigne bool DEFAULT false,
CONSTRAINT pas_deux_fois_le_meme_bouton UNIQUE (label,categorie));

CREATE TABLE aliases
(id SERIAL PRIMARY KEY,
alias varchar DEFAULT '' CONSTRAINT alias_vide_interdit CHECK (alias != ''),
idbde int DEFAULT -100 CONSTRAINT idbde_doit_etre_specifie CHECK (idbde!=(-100)),
UNIQUE (alias));

CREATE TABLE historique
(id SERIAL PRIMARY KEY,
idbde int DEFAULT -100 CONSTRAINT idbde_doit_etre_specifie CHECK (idbde!=(-100)),
avant varchar DEFAULT '' CONSTRAINT pseudo_avant_vide_impossible CHECK (avant != ''),
apres varchar DEFAULT '' CONSTRAINT pseudo_apres_vide_impossible CHECK (apres != ''),
date timestamp DEFAULT NULL CONSTRAINT date_doit_etre_specifiee CHECK (NOT date is NULL),
valide bool DEFAULT false);

CREATE TABLE activites
(id SERIAL PRIMARY KEY,
 debut timestamp NOT NULL,
 fin timestamp NOT NULL CONSTRAINT activite_a_une_duree_positive CHECK (debut<=fin),
 titre varchar DEFAULT '' CONSTRAINT titre_doit_etre_specifie CHECK (titre!=''),
 lieu varchar DEFAULT '',
 description varchar DEFAULT '',
 responsable int DEFAULT 0 CONSTRAINT responsable_doit_etre_specifie CHECK (responsable!=0),
 signature varchar DEFAULT '',
 validepar int DEFAULT -100,
 liste bool DEFAULT false,
 en_cours bool DEFAULT false,
 listeimprimee bool DEFAULT false);

CREATE TABLE entree_activites
(id SERIAL PRIMARY KEY,
 heure_entree timestamp NOT NULL,
 activite int DEFAULT (-1) CONSTRAINT activite_doit_etre_specifie CHECK (activite!=(-1)),
 est_invite bool DEFAULT false,
 idbde int DEFAULT -100 CONSTRAINT idbde_doit_etre_specifie CHECK (idbde!=(-100) OR est_invite),
 responsable int DEFAULT (-100) CONSTRAINT responsable_doit_etre_specifie CHECK (responsable!=(-100) OR NOT est_invite)
 );


CREATE TABLE invites
(id SERIAL PRIMARY KEY,
 nom varchar DEFAULT '' CONSTRAINT nom_doit_etre_specifie CHECK (nom!=''),
 prenom varchar DEFAULT '' CONSTRAINT prenom_doit_etre_specifie CHECK (prenom!=''),
 responsable int DEFAULT (-1) CONSTRAINT responsable_doit_etre_specifie CHECK (responsable!=(-1)),
 activite int DEFAULT (-1) CONSTRAINT activite_doit_etre_specifie CHECK (activite!=(-1))
 );

CREATE TABLE wei_vieux
(idwei serial PRIMARY KEY,
 idbde integer REFERENCES comptes UNIQUE,
 nom varchar(255) NOT NULL,
 prenom varchar(255) NOT NULL,
 tel varchar(20) NOT NULL,
 urgence_nom varchar(255) NOT NULL,
 urgence_tel varchar(20) NOT NULL,
 mail varchar(254) NOT NULL,
 annee integer NOT NULL,
 dept varchar(255) NOT NULL,
 paiement varchar(255) NOT NULL,
 normalien boolean NOT NULL,
 pseudo varchar(255) NOT NULL,
 pbsante text,
 bus varchar(255),
 role text,
 payé boolean DEFAULT FALSE NOT NULL,
 caution boolean DEFAULT FALSE NOT NULL,
 "conge_valide" CHECK (conge = FALSE OR normalien = true),
 readhere boolean DEFAULT FALSE NOT NULL
 );

CREATE TABLE wei_1a
(idwei serial PRIMARY KEY,
 nom varchar(255) NOT NULL,
 prenom varchar(255) NOT NULL,
 adresse varchar NOT NULL,
 mail varchar(254) NOT NULL,
 normalien boolean NOT NULL DEFAULT FALSE,
 tel varchar(20) NOT NULL,
 urgence_nom varchar(255) NOT NULL,
 urgence_tel varchar(20) NOT NULL,
 dept varchar(255) NOT NULL,
 note varchar NOT NULL,
 infos varchar DEFAULT '' NOT NULL,
 q_softs varchar NOT NULL,
 q_sports varchar NOT NULL,
 q_alcools varchar NOT NULL,
 q_musiques varchar NOT NULL,
 q_soirees varchar NOT NULL,
 q_personnages varchar NOT NULL,
 q_jeux varchar NOT NULL,
 soge boolean DEFAULT TRUE NOT NULL,
 sexe varchar(255) DEFAULT 'M' NOT NULL,
 adhere boolean NOT NULL,
 triggered boolean NOT NULL
 );

CREATE OR REPLACE FUNCTION start_end_year (annee int DEFAULT 0, OUT debut date,OUT fin date) RETURNS record
--- Renvoie le début et la fin de l'annee ``annee`` pour savoir sur quelle période quelqu'un ayant
--- adhéré peut utiliser son compte.
AS $BODY$
DECLARE snym int;
        snyd int;
        epym int;
        epyd int;
        ybm int;
        ybd int;
BEGIN
  SELECT start_next_year_month,start_next_year_day,end_previous_year_month,end_previous_year_day,year_boot_month,year_boot_day
         INTO snym,snyd,epym,epyd,ybm,ybd
         FROM configurations
         WHERE used='t';
  IF (annee=0)
  THEN
      annee:=EXTRACT(year FROM current_date);
      IF (current_date<CAST(annee||'-'||ybm||'-'||ybd AS date))  ---si on est avant le year boot, c'est qu'on est encore l'année dernière
      THEN
          annee:=annee-1;
      END IF;
  END IF;
  debut:=CAST(annee||'-'||snym||'-'||snyd AS date);
  fin:=CAST((annee+1)||'-'||epym||'-'||epyd AS date);
END
$BODY$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION previous_year_boot (date date DEFAULT NULL, OUT boot date) RETURNS date
--- Renvoie le dernier year_boot précédant ``date``. (Si NULL, on prend NOW()).
AS $BODY$
DECLARE ybm int;
        ybd int;
BEGIN
  SELECT year_boot_month,year_boot_day
         INTO ybm,ybd
         FROM configurations
         WHERE used='t';
  IF (date is NULL)
  THEN
      date:=NOW();
  END IF;
  boot:=CAST(EXTRACT(year FROM date) || '-' || ybm || '-' || ybd AS date);
  IF (boot > date)
  THEN
      --- Si on est entre janvier et ybm, le year_boot calculé est dans le futur, or on veut le précédent
      boot:=CAST(EXTRACT(year FROM date)-1 || '-' || ybm || '-' || ybd AS date);
  END IF;
END
$BODY$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION isAdherent (idbdeask int, date date DEFAULT now(), OUT answer bool, OUT section varchar, OUT section_year int)
RETURNS record
AS $BODY$
DECLARE debut date;
        fin date;
        annee int;
        typ varchar;
BEGIN
  SELECT type INTO typ FROM comptes WHERE idbde=idbdeask;
  IF typ='special' THEN
    answer:='t';
    section:='Special';
    RETURN;
  END IF;
  IF typ='club' THEN
    answer:='t';
    section:='Club';
    RETURN;
  END IF;
  SELECT adhesions.annee,adhesions.section INTO annee, section
         FROM adhesions
         WHERE id=(SELECT last_adhesion FROM comptes WHERE idbde=idbdeask);
  IF annee is NULL THEN
    answer:='f';
    section:='Unknown';
    section_year:=-1;
    RETURN;
  END IF;
  section_year:=annee;
  SELECT foo.debut,foo.fin INTO debut,fin FROM start_end_year(annee) AS foo;
  IF (debut<=date) AND (date<=fin) THEN
    answer:='t';
  ELSE
    answer:='f';
  END IF;
END
$BODY$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION section (idbde int DEFAULT -100) RETURNS varchar
AS $BODY$
BEGIN
  IF (idbde=(-100))
  THEN
      RETURN 'Unknown';
  ELSE
      RETURN (SELECT section FROM isAdherent(idbde));
  END IF;
END
$BODY$ LANGUAGE plpgsql;

-------------------------------------------------------------------------------
--- Maintenant on met des trucs dans les tables histoire que ce soit un peu ---
---      moins tristounet ... et surtout qu'on puisse bidouiller un peu     ---  
-------------------------------------------------------------------------------

--- Table comptes

--- NB, avant d'inserer des idbde <=0, il faut mettre au moins une personne
--- sinon, le trigger qui cherche le max(idbde) essaye de set la valeur à un truc <=0
--- et ça plante puisque il est enregistre que sa minvalue est 1
INSERT INTO comptes
(pseudo,passwd,nom,prenom,mail,normalien,idcrans,droits,surdroits,supreme,last_adhesion) VALUES
('20-100','7wqqjiuz|b7d14db15f33baa53180b4e3da489a92388e38a3eb8f5f17a722a32f1d17c36b','Le Gallic','Vincent',
'legallic@crans.org',true,3658,'all','all',true,1);
--- ex-compte de Basile, passé special user
--- on le garde empty pour pas tout décaler
INSERT INTO comptes
(pseudo,passwd,nom,prenom,mail,normalien,idcrans,droits,surdroits,supreme) VALUES
('empty','|','Empty','Empty',
'nobody@crans.org',true,-1,'','',false);
INSERT INTO comptes
(pseudo,passwd,nom,prenom,mail,normalien,idcrans,droits,surdroits,supreme,last_adhesion) VALUES
('ctrlshiftu03b1','erutkk78|d34a93abc9f735410ff0b1b6889034c81543ddfd5ae7f30a76d3b24aa941aec0', 'Blockelet','Michel',
'blockelet@crans.org',true,-1,'all','all',true,3);
INSERT INTO comptes (idbde,type,pseudo,passwd,nom,prenom,mail,adresse,sexe,fonction,idcrans,droits,surdroits,supreme) VALUES (0,'club','Bde','*|*','Bde','Bde','bde@crans.org','61 avenue du président Wilson','','Bureau des élèves',1,'','',false);
INSERT INTO comptes
(idbde,type,pseudo,passwd,solde,nom,prenom,mail,adresse,sexe,fonction,normalien,droits,commentaire) VALUES
(-1,'special','Cheques','*|*',0,'Chèques','Note Spéciale','nomail-bde@crans.org','','S','être l''opposé de la somme des chèques crédités','f','','Il ne vaut mieux pas toucher à ce compte...');
INSERT INTO comptes
(idbde,type,pseudo,passwd,solde,nom,prenom,mail,adresse,sexe,fonction,normalien,droits,commentaire) VALUES
(-2,'special','Especes','*|*',0,'Espèces','Note Spéciale','nomail-bde@crans.org','','S','être l''opposé de la somme des espèces crédités','f','','Il ne vaut mieux pas toucher à ce compte...');
INSERT INTO comptes
(idbde,type,pseudo,passwd,solde,nom,prenom,mail,adresse,sexe,fonction,normalien,droits,commentaire) VALUES
(-3,'special','Virements','*|*',0,'Virements','Note Spéciale','nomail-bde@crans.org','','S','être l''opposé de la somme des virements crédités','f','','Il ne vaut mieux pas toucher à ce compte...');

--- pour pouvoir faire mumuse avec toto (mdp = plop)
INSERT INTO comptes (pseudo,passwd,nom,prenom,mail) VALUES
('toto','7wqqjiuz|b7d14db15f33baa53180b4e3da489a92388e38a3eb8f5f17a722a32f1d17c36b','Passoir','Toto','toto@crans.org');
--- et puis on veut aussi pouvoir faire joujou avec un club
INSERT INTO comptes (type,pseudo,passwd,nom,prenom,mail) VALUES
('club','clubtest','ahf5D|20245ad76d1ce1cbca782268d2702fc9','Test','Club','clubtest@nowhere.org');

INSERT INTO comptes
(pseudo,passwd,nom,prenom,mail,normalien,idcrans,droits,surdroits,supreme,last_adhesion) VALUES
('peb','opx5jf32|8efdd2d5880ff8502cf41d7990fc4a6a32d4040f99fe65858dfae11be409de0a','Bécue','Pierre-Elliott',
'becue@crans.org',false,-1,'all','all',true,4);
INSERT INTO comptes
(pseudo,passwd,nom,prenom,mail,normalien,idcrans,droits,surdroits,supreme,last_adhesion) VALUES
('quelu','4hg6kl90|9527c15ac1481495e03ca029fd3ca6f486e5136f8654345d7be82366b65936c9','Pellissier','Luc',
'pellissier@crans.org',true,-1,'all','all',true,5);
INSERT INTO comptes
(pseudo,passwd,nom,prenom,mail,normalien,idcrans,droits,surdroits,supreme,last_adhesion) VALUES
('nek','z0wv1e3i|0fc0ba4e41a5ad68265d7476f0b53ed44fc3f0c349b7b1e25b339eca3f10cc51','Moisy-Mabille','Kévin',
'nek@crans.org',true,-1,'all','',false,6);
INSERT INTO comptes
(pseudo,passwd,nom,prenom,mail,normalien,idcrans,droits,surdroits,supreme,last_adhesion) VALUES
('pika','huh0zm01|5451ff93a61889d1d4a9f668aa4a0659810173e2b0238544e0c2116abcf560a9','Duplouy','Yann',
'pika@crans.org',true,-1,'all','all',true,7);
INSERT INTO comptes
(pseudo,passwd,nom,prenom,mail,normalien,idcrans,droits,surdroits,supreme,last_adhesion) VALUES
('shadoko','3fu76f5r|3c8189200eb7db1166cab144435533af813014550d9f9a0cfc020af98b2bd662','Baste','Julien',
'shadoko@crans.org',true,-1,'all','',false,8);
INSERT INTO comptes
(pseudo,passwd,nom,prenom,mail,normalien,idcrans,droits,surdroits,supreme,last_adhesion,sexe) VALUES
('chopopope','sr4hb3f4|2d35d2c5052bac878f27fdd61e7fb9e2731aebe323e400139a6f7b7d9f370aaf','Pommeret','Pauline',
'chopopope@crans.org',true,-1,'all','',false,9,'F');
INSERT INTO comptes
(pseudo,passwd,nom,prenom,mail,normalien,idcrans,droits,surdroits,supreme,last_adhesion) VALUES
('tudor','4t5gh7d4|c20632297bcbf92c774188c1d790d2001d90d2606e27064300c006f8e2b6b3f5','Stan','Daniel',
'dstan@crans.org',true,-1,'all','',false,10);
INSERT INTO comptes
(pseudo,passwd,nom,prenom,mail,normalien,idcrans,droits,surdroits,supreme,last_adhesion) VALUES
('skippy','7wqqjiuz|b7d14db15f33baa53180b4e3da489a92388e38a3eb8f5f17a722a32f1d17c36b','Lajus','Jonathan',
'skippy@crans.org',true,-1,'all','all',true,11);
INSERT INTO comptes
(pseudo,passwd,nom,prenom,mail,normalien,idcrans,droits,surdroits,supreme,last_adhesion) VALUES
('olasd','7wqqjiuz|b7d14db15f33baa53180b4e3da489a92388e38a3eb8f5f17a722a32f1d17c36b','Dandrimont','Nicolas',
'olasd@crans.org',true,-1,'all','all',true,12);
INSERT INTO comptes
(pseudo,passwd,nom,prenom,mail,normalien,idcrans,droits,surdroits,supreme,last_adhesion) VALUES
('kevs','7wqqjiuz|b7d14db15f33baa53180b4e3da489a92388e38a3eb8f5f17a722a32f1d17c36b','Viard','Kévin',
'kevs@crans.org',true,-1,'all','all',true,13);

UPDATE comptes SET sexe='C' WHERE type='club';

--- maintenant qu'on a mis des bêtises dedans dans le désordre, on les réordonne
CLUSTER index_idbde ON comptes;
--- Quelques alias, histoire de pouvoir les tester
INSERT INTO aliases (alias,idbde) VALUES ('plouf',1);
INSERT INTO aliases (alias,idbde) VALUES ('plop',1);
INSERT INTO aliases (alias,idbde) VALUES ('michou',2);

--- Pareil, avec historique
INSERT INTO historique (idbde,avant,apres,date,valide) VALUES (1,'vincentl','vincent','2011-12-31',true);
INSERT INTO historique (idbde,avant,apres,date,valide) VALUES (1,'valérian','vincent','2012-01-01',false);
INSERT INTO historique (idbde,avant,apres,date,valide) VALUES (1,'vincent','vincent','2012-01-01 133400',true);
INSERT INTO historique (idbde,avant,apres,date,valide) VALUES (2,'michelb','michou','2012-01-01 133400',true);

--- Table adhesions
--- parce que ce serait cool que les comptes existants aient adhéré
INSERT INTO adhesions (idbde,annee,wei,idtransaction,section) VALUES (1,2012,'t',2,'2A♡');
INSERT INTO adhesions (idbde,annee,wei,idtransaction,section) VALUES (1,2013,'t',4,'3A♡');
INSERT INTO adhesions (idbde,annee,wei,idtransaction,section) VALUES (3,2013,'t',6,'5A0');
INSERT INTO adhesions (idbde,annee,wei,idtransaction,section) VALUES (6,2012,'t',8,'2A♡');
INSERT INTO adhesions (idbde,annee,wei,idtransaction,section) VALUES (7,2012,'t',10,'2A01♡');
INSERT INTO adhesions (idbde,annee,wei,idtransaction,section) VALUES (8,2012,'t',12,'3.5A♡');
INSERT INTO adhesions (idbde,annee,wei,idtransaction,section) VALUES (9,2012,'t',14,'1A♡');
INSERT INTO adhesions (idbde,annee,wei,idtransaction,section) VALUES (10,2012,'t',16,'2A♡');
INSERT INTO adhesions (idbde,annee,wei,idtransaction,section) VALUES (11,2012,'t',18,'1A3♡');
INSERT INTO adhesions (idbde,annee,wei,idtransaction,section) VALUES (12,2012,'t',20,'2A♡');
INSERT INTO adhesions (idbde,annee,wei,idtransaction,section) VALUES (13,2012,'t',22,'1A♡');
INSERT INTO adhesions (idbde,annee,wei,idtransaction,section) VALUES (14,2012,'t',24,'5A''2');
INSERT INTO adhesions (idbde,annee,wei,idtransaction,section) VALUES (14,2012,'t',26,'2B3');

--- Table transactions
--- parce que c'est encore plus cool si ces gens ont payé pour s'inscrire
INSERT INTO transactions (type,emetteur,destinataire,montant,description)
     VALUES ('crédit',-2,1,15000,'crédit espèces');
INSERT INTO transactions (type,emetteur,destinataire,montant,description,cantinvalidate)
     VALUES ('transfert',1,0,15000,'Adhésion de 1 pour 2011',true);
INSERT INTO transactions (type,emetteur,destinataire,montant,description)
     VALUES ('crédit',-2,1,15000,'crédit espèces');
INSERT INTO transactions (type,emetteur,destinataire,montant,description,cantinvalidate)
     VALUES ('transfert',1,0,15000,'Adhésion de 1 pour 2012',true);
INSERT INTO transactions (type,emetteur,destinataire,montant,description)
     VALUES ('crédit',-2,2,15000,'crédit espèces');
INSERT INTO transactions (type,emetteur,destinataire,montant,description,cantinvalidate)
     VALUES ('transfert',2,0,15000,'Adhésion de 2 pour 2012',true);
INSERT INTO transactions (type,emetteur,destinataire,montant,description)
     VALUES ('crédit',-2,6,15000,'crédit espèces');
INSERT INTO transactions (type,emetteur,destinataire,montant,description,cantinvalidate)
     VALUES ('transfert',6,0,15000,'Adhésion de 6 pour 2011',true);
INSERT INTO transactions (type,emetteur,destinataire,montant,description)
     VALUES ('crédit',-2,7,15000,'crédit espèces');
INSERT INTO transactions (type,emetteur,destinataire,montant,description,cantinvalidate)
     VALUES ('transfert',7,0,15000,'Adhésion de 7 pour 2011',true);
INSERT INTO transactions (type,emetteur,destinataire,montant,description)
     VALUES ('crédit',-2,8,15000,'crédit espèces');
INSERT INTO transactions (type,emetteur,destinataire,montant,description,cantinvalidate)
     VALUES ('transfert',8,0,15000,'Adhésion de 8 pour 2011',true);
INSERT INTO transactions (type,emetteur,destinataire,montant,description)
     VALUES ('crédit',-2,9,15000,'crédit espèces');
INSERT INTO transactions (type,emetteur,destinataire,montant,description,cantinvalidate)
     VALUES ('transfert',9,0,15000,'Adhésion de 9 pour 2011',true);
INSERT INTO transactions (type,emetteur,destinataire,montant,description)
     VALUES ('crédit',-2,10,15000,'crédit espèces');
INSERT INTO transactions (type,emetteur,destinataire,montant,description,cantinvalidate)
     VALUES ('transfert',10,0,15000,'Adhésion de 10 pour 2011',true);
INSERT INTO transactions (type,emetteur,destinataire,montant,description)
     VALUES ('crédit',-2,11,15000,'crédit espèces');
INSERT INTO transactions (type,emetteur,destinataire,montant,description,cantinvalidate)
     VALUES ('transfert',11,0,15000,'Adhésion de 11 pour 2011',true);
INSERT INTO transactions (type,emetteur,destinataire,montant,description)
     VALUES ('crédit',-2,12,15000,'crédit espèces');
INSERT INTO transactions (type,emetteur,destinataire,montant,description,cantinvalidate)
     VALUES ('transfert',12,0,15000,'Adhésion de 12 pour 2011',true);
INSERT INTO transactions (type,emetteur,destinataire,montant,description)
     VALUES ('crédit',-2,13,15000,'crédit espèces');
INSERT INTO transactions (type,emetteur,destinataire,montant,description,cantinvalidate)
     VALUES ('transfert',13,0,15000,'Adhésion de 13 pour 2011',true);
INSERT INTO transactions (type,emetteur,destinataire,montant,description)
     VALUES ('crédit',-2,14,15000,'crédit espèces');
INSERT INTO transactions (type,emetteur,destinataire,montant,description,cantinvalidate)
     VALUES ('transfert',14,0,15000,'Adhésion de 14 pour 2011',true);
INSERT INTO transactions (type,emetteur,destinataire,montant,description)
     VALUES ('crédit',-2,15,15000,'crédit espèces');
INSERT INTO transactions (type,emetteur,destinataire,montant,description,cantinvalidate)
     VALUES ('transfert',15,0,15000,'Adhésion de 15 pour 2011',true);
--- Du coup il faut que les soldes soient cohérents
UPDATE comptes SET solde=-195000 WHERE idbde=-2;
UPDATE comptes SET solde=195000 WHERE idbde=0;

--- Table configurations
--- on enregistre la configuration par défaut (qui est used, obviously)
INSERT INTO configurations (used,nom,
prix_wei_normalien, prix_wei_non_normalien, prix_adhesion,
solde_negatif, solde_tres_negatif, solde_pas_plus_negatif,
start_next_year_month, start_next_year_day,
end_previous_year_month, end_previous_year_day,
year_boot_month, year_boot_day,
historique_pseudo_timeout, historique_incompressible,
liste_invites_opening_time, liste_invites_closing_time, max_invitation_par_personne, max_invitation_par_an,
toggle_transaction_timeout)
VALUES ('t','default',
15000, 13500, 4000,
0, -4000, -20000,
04, 30,
11, 30,
09, 01,
365, 24,
672, 5, 3, 5,
604800);


--- Table boutons
--INSERT INTO boutons (label,montant,destinataire,categorie)
--VALUES ('Coca',90,0,'Soft');
--INSERT INTO boutons (label,montant,destinataire,categorie)
--VALUES ('Burger',150,0,'Bouffe');
--INSERT INTO boutons (label,montant,destinataire,categorie,affiche)
--VALUES ('Useless',0,0,'Autre',false);


--- Table activites
INSERT INTO activites (debut,fin,titre,lieu,description,responsable,signature)
VALUES ('2012-03-13 21:00:00+01:00','2012-03-14 03:00:00+01:00','[Pot] 1D2','Kfet','',2882,'1D2');
INSERT INTO activites (debut,fin,titre,lieu,description,responsable,signature)
VALUES ('2012-03-14 20:00:00+01:00','2012-03-15 00:00:00+01:00','Atelier d''écriture P[ENS]il','Kfet','',3357,'P[ENS]il et atelier d''écriture');
INSERT INTO activites (debut,fin,titre,lieu,description,responsable,signature)
VALUES ('2012-03-15 20:30:00+01:00','2012-03-15 22:30:00+01:00','LIKA vs Polytechnique','Curie','',2430,'');
INSERT INTO activites (debut,fin,titre,lieu,description,responsable,signature)
VALUES ('2012-03-24 21:00:00+01:00','2012-03-25 03:00:00+02:00','Pot 1A2','Kfet-kokarde','',3220,'Amélie Klein');
INSERT INTO activites (debut,fin,titre,lieu,description,responsable,signature)
VALUES ('2012-03-29 20:00:00+02:00','2012-03-30 00:00:00+02:00','Diner de l''Aspique','Kfet','Utilisation de la cuisine l''après-midi',3357,'L''Aspique');
INSERT INTO activites (debut,fin,titre,lieu,description,responsable,signature)
VALUES ('2012-03-29 20:30:00+02:00','2012-03-29 23:00:00+02:00','LIKA vs Polytechnique','PDJ','',2430,'LIKA');
INSERT INTO activites (debut,fin,titre,lieu,description,responsable,signature)
VALUES ('2012-03-30 19:00:00+02:00','2012-03-30 21:00:00+02:00','Projection par les 1D3','Kfet','',3357,'les 1D3');
INSERT INTO activites (debut,fin,titre,lieu,description,responsable,signature)
VALUES ('2012-04-02 20:00:00+02:00','2012-04-03 00:00:00+02:00','Soirée Kitch[ens]','Kfet','',2969,'Club Kitch[ens]');
INSERT INTO activites (debut,fin,titre,lieu,description,responsable,signature)
VALUES ('2012-04-03 21:00:00+02:00','2012-04-04 05:00:00+02:00','Pot 3D2  ','K-fêt + Kokarde','[POT]hèse PPP',2558,'Sarah');
INSERT INTO activites (debut,fin,titre,lieu,description,responsable,signature)
VALUES ('2012-04-05 20:30:00+02:00','2012-04-06 00:00:00+02:00','Soirée Club Bière','Kfet','',2870,'binouze');
INSERT INTO activites (debut,fin,titre,lieu,description,responsable,signature)
VALUES ('2012-04-07 20:30:00+02:00','2012-04-07 23:59:00+02:00','Les Douze Trav[POT] d''Hercule','KFet','',3269,'Le BdS');
INSERT INTO activites (debut,fin,titre,lieu,description,responsable,signature)
VALUES ('2012-04-09 20:00:00+02:00','2012-04-09 23:59:00+02:00','Soirée Nanar','Kfet','Soirée Nanar ',2927,'On aime les Nanar!');
INSERT INTO activites (debut,fin,titre,lieu,description,responsable,signature)
VALUES ('2012-04-10 21:00:00+02:00','2012-04-11 03:00:00+02:00','Pot 1D3','Kfet puis peut etre kokarde','',2804,'');
INSERT INTO activites (debut,fin,titre,lieu,description,responsable,signature)
VALUES ('2012-04-11 18:30:00+02:00','2012-04-11 20:30:00+02:00','Débat présidentielle 2012','Amphi Tocqueville','',2056,'Yacine Baouch');
INSERT INTO activites (debut,fin,titre,lieu,description,responsable,signature)
VALUES ('2012-04-11 20:00:00+02:00','2012-04-12 00:00:00+02:00','Soirée Sda','Kfet','',3220,'Les sens de l''art');
INSERT INTO activites (debut,fin,titre,lieu,description,responsable,signature)
VALUES ('2012-04-12 20:30:00+02:00','2012-04-12 22:30:00+02:00','LIKA vs Medecine','Curie','',2430,'');
INSERT INTO activites (debut,fin,titre,lieu,description,responsable,signature)
VALUES ('2012-05-05 17:00:00+02:00','2012-05-05 21:00:00+02:00','Championnat de France de GO des Jeunes 1','Kfet','Cuisine utilisée',3272,'BdL');
INSERT INTO activites (debut,fin,titre,lieu,description,responsable,signature)
VALUES ('2012-05-06 08:00:00+02:00','2012-05-06 09:00:00+02:00','Championnat de France de GO des Jeunes 2','Kfet','Cuisine utilisée',3272,'BdL');
INSERT INTO activites (debut,fin,titre,lieu,description,responsable,signature)
VALUES ('2012-05-08 21:00:00+02:00','2012-05-09 03:00:00+02:00','La [pot]hèse PIP','Kfet + kokarde ?','',3357,'Les 3D2');
INSERT INTO activites (debut,fin,titre,lieu,description,responsable,signature)
VALUES ('2012-05-26 20:00:00+02:00','2012-05-26 23:59:00+02:00','Soirée Eurovisio[ENS]','Kfet','',3243,'Eurovisio[ENS]');
--- histoire qu'elles soient utilisables
UPDATE activites SET validepar=1;
UPDATE activites SET liste=true WHERE id%2=0;
UPDATE activites SET debut=debut+CAST('5months' AS interval),fin=fin+CAST('5months' AS interval);
