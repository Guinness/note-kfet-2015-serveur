#!/bin/bash

# A exécuter en root

NOTEUSER="note"

if [[ $USER == root ]]
then
 service postgresql restart

 echo ""
 echo "Creating database"
 sudo -u postgres psql -f .recreate_database.sql -d postgres

 echo ""
 echo "Creating tables"
 #sudo -u note psql note -f .recreate_tables.sql -d note #2>&1 | grep -A 2 "ERREUR" --color
 sudo -u $NOTEUSER psql note -f .recreate_tables_empty.sql -d note 2>&1 | sed 's/\(ERREUR\)/[1;31m\1[0m/g'


 echo "done"
else
 echo "Doit être exécuté en tant que root. (Et root doit avoir l'accès à la base pgsql en SUPERUSER)."
fi
