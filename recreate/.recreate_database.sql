--- Drop la base note et la recrée. Ainsi que le role note
--- NB : A exécuter depis une base qui ne soit pas note

DROP DATABASE note;
--- on ne peut plus DROP note parce qu'il est nécessaire à la base django_client. Nevermind.
--- DROP ROLE note;

CREATE DATABASE note;
ALTER DATABASE note OWNER TO note;
--- CREATE ROLE note LOGIN;

--- NB : après ce script, il faut exéuter recreate_tables.sql
---      sur la base note fraîchement créée
