#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
import sys
import psycopg2
import psycopg2.extras


db_nk12 = "bde"
db_nk15 = "note"

DEBUG = False;

def getcursor(db):
    """Renvoie une connexion à la base demandé et un curseur"""
    con = psycopg2.connect(database=db)
    cur = con.cursor(cursor_factory=psycopg2.extras.DictCursor)
    return con, cur



def import_adherents():
    con_old, cur_old = getcursor(db_nk12)
    con_new, cur_new = getcursor(db_nk15)
    cur_old.execute("""SELECT * FROM adherents order by numcbde;""")
    adherents = cur_old.fetchall()
    # adherents[0] correspond a nobody
    # adherents[1] à espèces
    # adherents[2] à bde 
    # Du coup, on commence à 3 pour l'importation, et on rajoutera après les notes spéciales
    for i in range(3, len(adherents)):
        adh = adherents[i]
        #Il faut faire attention au type
        if adh['club']:
            adh_type = "club"
        else:
            adh_type = "personne"  
        #Le solde est maintenant en centimes et est un integer
        adh_solde = int(100*adh['solde'])
        #On formate bien comme il faut la liste d'un adherent
        adherent = [adh['numcbde'],adh_type,adh['pseudo'],adh_solde,adh['nom'],adh['prenom'],adh['telephone'],adh['email'],adh['adresse'],adh['sexe'],adh['fonction'],adh['normalien'],adh['pbsante'],adh['supreme'],adh['bloque'] ]
        if DEBUG:
            print adherent
        #Puis on l'insère
        cur_new.execute( """INSERT INTO comptes (idbde,type,pseudo,solde,nom,prenom,tel,mail,adresse,sexe,fonction,normalien,pbsante,supreme,bloque) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s);""", adherent)
    #Il manquera last_adhesion a rajouter après.
    cur_new.execute("COMMIT;")
    #On rajoute les comptes spéciaux
    cur_new.execute("""INSERT INTO comptes (idbde,type,pseudo,passwd,nom,prenom,mail,adresse,sexe,fonction,droits,surdroits,supreme) VALUES (0,'club','Bde','*|*','Bde','Bde','bde@crans.org','61 avenue du président Wilson','','Bureau des élèves','','',false);
INSERT INTO comptes
(idbde,type,pseudo,passwd,solde,nom,prenom,mail,adresse,sexe,fonction,normalien,droits,commentaire) VALUES
(-1,'special','Cheques','*|*',0,'Chèques','Note Spéciale','nomail-bde@crans.org','','S','être l''opposé de la somme des chèques crédités','f','','Il ne vaut mieux pas toucher à ce compte...');
INSERT INTO comptes
(idbde,type,pseudo,passwd,solde,nom,prenom,mail,adresse,sexe,fonction,normalien,droits,commentaire) VALUES
(-2,'special','Especes','*|*',0,'Espèces','Note Spéciale','nomail-bde@crans.org','','S','être l''opposé de la somme des espèces crédités','f','','Il ne vaut mieux pas toucher à ce compte...');
INSERT INTO comptes
(idbde,type,pseudo,passwd,solde,nom,prenom,mail,adresse,sexe,fonction,normalien,droits,commentaire) VALUES
(-3,'special','Virements','*|*',0,'Virements','Note Spéciale','nomail-bde@crans.org','','S','être l''opposé de la somme des virements crédités','f','','Il ne vaut mieux pas toucher à ce compte...');
INSERT INTO comptes
(idbde,type,pseudo,passwd,solde,nom,prenom,mail,adresse,sexe,fonction,normalien,droits,commentaire) VALUES
(-4,'special','Carte bleu','*|*',0,'Carte bleu','Note Spéciale','nomail-bde@crans.org','','S','être l''opposé de la somme des cb crédités','f','','Il ne vaut mieux pas toucher à ce compte...');
UPDATE comptes set passwd='toto|871872670134933f1bc52cde8043cb429efcdab63266dda1cc27244ee107a505' where idbde = '3962';""") #mot de passe = toto pour ceux qui veulent
    cur_new.execute("COMMIT;")
    cur_new.execute("CLUSTER index_idbde ON comptes;COMMIT;")


def import_adhesions():
    con_old, cur_old = getcursor(db_nk12)
    con_new, cur_new = getcursor(db_nk15)
    cur_old.execute("""SELECT * FROM inscriptions;""")
    # On trie par annee décroissante, comme ça on peut remplir last_adhesion la première fois, puis on rempli adherents[idbe] avec la section du mec, pour la réutiliser les prochaines fois
    adhesions = cur_old.fetchall()
    for i in range(0, len(adhesions)):
        adh = adhesions[i]
        cur_new.execute("""UPDATE comptes SET last_adhesion = %s WHERE idbde = %s;""", [adh['id'],adh['adherent']])
        cur_old.execute(""" SELECT * FROM adherents WHERE numcbde = %s""", [adh['adherent']])
        section = cur_old.fetchone()['section']
        if section == '':
            section = 'None'
        adhesion = [adh['id'],adh['adherent'],adh['annee'],adh['wei'],adh['transaction'],adh['annee'],section]
        if DEBUG:
            print adhesion
        cur_new.execute( """INSERT INTO adhesions (id,idbde,annee,wei,idtransaction,date,section) VALUES (%s,%s,%s,%s,%s,'%s-09-01',%s);""", adhesion)
    cur_new.execute("""COMMIT;""")

def update_indexes():
    con_old, cur_old = getcursor(db_nk12)
    con_new, cur_new = getcursor(db_nk15)
    cur_new.execute("""SELECT * from adhesions order by id desc;""")
    last_id = cur_new.fetchone()['id']
    cur_new.execute("""ALTER SEQUENCE adhesions_id_seq RESTART WITH %s;""", [last_id+1,])
    cur_new.execute("""SELECT * from transactions order by id desc;""")
    last_id = cur_new.fetchone()['id']
    cur_new.execute("""ALTER SEQUENCE transactions_id_seq RESTART WITH %s;""", [last_id+1,])
    cur_new.execute("""SELECT * from boutons order by id desc;""")
    last_id = cur_new.fetchone()['id']
    cur_new.execute("""ALTER SEQUENCE boutons_id_seq RESTART WITH %s;""", [last_id+1,])
    cur_new.execute("""SELECT * from activites order by id desc;""")
    last_id = cur_new.fetchone()['id']
    cur_new.execute("""ALTER SEQUENCE activites_id_seq RESTART WITH %s;""", [last_id+1,])
    cur_new.execute("""SELECT * from cheques order by id desc;""")
    last_id = cur_new.fetchone()['id']
    cur_new.execute("""ALTER SEQUENCE cheques_id_seq RESTART WITH %s;""", [last_id+1,])
    cur_new.execute("""SELECT * from invites order by id desc;""")
    last_id = cur_new.fetchone()['id']
    cur_new.execute("""ALTER SEQUENCE invites_id_seq RESTART WITH %s;""", [last_id+1,])
    cur_new.execute("""COMMIT;""")

def import_transactions():
    con_old, cur_old = getcursor(db_nk12)
    con_new, cur_new = getcursor(db_nk15)
    cur_old.execute("""SELECT * FROM transactions;""")
    # On trie par annee décroissante, comme ça on peut remplir last_adhesion la première fois, puis on rempli adherents[idbe] avec la section du mec, pour la réutiliser les prochaines fois
    trans = cur_old.fetchall()
    length = len(trans)
    for i in range(0, length):
        remarque = trans[i]['remarque']
        if (remarque=="crédit espèce") or (remarque =="cheque") or (remarque == "virement bancaire") or (remarque=="carte bancaire"):
            trans_type = "crédit"
        elif remarque =="retrait d'espèces":
            trans_type ="retrait"
        elif "transfert" in remarque:
            trans_type ="transfert"
        else:
            trans_type ="bouton"
        cur_new.execute( """INSERT INTO transactions (id,date,type,emetteur,destinataire,quantite,montant,description,valide,cantinvalidate) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s);""", [trans[i]['id'],trans[i]['date'],trans_type,trans[i]['emetteur'],trans[i]['destinataire'],trans[i]['quantite'],int(100*trans[i]['montant']),trans[i]['remarque'],trans[i]['valide'],trans[i]['locked']])
        if DEBUG:
            print [i,length]
    cur_new.execute("""COMMIT;""")


def import_activites():
    con_old, cur_old = getcursor(db_nk12)
    con_new, cur_new = getcursor(db_nk15)
    cur_old.execute("""SELECT * FROM activites""")
    act = cur_old.fetchall()
    length = len(act)
    for i in range(0, length):
        cur_new.execute( """INSERT INTO activites (id,debut,fin,titre,lieu,description,responsable,signature,validepar,liste) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s);""", [act[i]['id'],act[i]['debut'],act[i]['fin'],act[i]['titre'],act[i]['lieu'],act[i]['descriptif'],act[i]['responsable'],act[i]['signature'],act[i]['validepar'],act[i]['list']])
        if DEBUG:
            print [i,length]
    cur_new.execute("""COMMIT;""")

def import_boutons():
    con_old, cur_old = getcursor(db_nk12)
    con_new, cur_new = getcursor(db_nk15)
    cur_old.execute("""SELECT * FROM boutons""")
    bout = cur_old.fetchall()
    length = len(bout)
    for i in range(0, length):
        cur_new.execute( """INSERT INTO boutons (id,label,montant,destinataire,categorie) VALUES (%s,%s,%s,%s,%s);""",[bout[i]['id'],bout[i]['label'],int(100*bout[i]['montant']),bout[i]['destinataire'],bout[i]['categorie']] )
        if DEBUG:
            print [i,length]
    cur_new.execute("""COMMIT;""")

def import_cheques():
    con_old, cur_old = getcursor(db_nk12)
    con_new, cur_new = getcursor(db_nk15)
    cur_old.execute("""SELECT * FROM cheques""")
    chq = cur_old.fetchall()
    length = len(chq)
    for i in range(0, length):
        if chq[i]['prenom']=='':
            prenom = 'Anonyme'
        else:
            prenom = chq[i]['prenom']
        if chq[i]['nom']=='':
            nom = 'Anonyme'
        else:
            nom = chq[i]['nom']
        cur_new.execute( """INSERT INTO cheques (id,date,nom,prenom,banque,montant,idtransaction) VALUES (%s,%s,%s,%s,%s,%s,%s);""", [chq[i]['id'],chq[i]['date'],nom,prenom,chq[i]['banque'],int(100*chq[i]['montant']),chq[i]['transaction']])
        if DEBUG:
            print [i,length]
    cur_new.execute("""COMMIT;""")

def import_invite():
    con_old, cur_old = getcursor(db_nk12)
    con_new, cur_new = getcursor(db_nk15)
    cur_old.execute("""SELECT * FROM invites""")
    inv = cur_old.fetchall()
    length = len(inv)
    for i in range(0, length):
        if inv[i]['prenom']=='' or inv[i]['nom']=='':
            continue
        cur_new.execute( """INSERT INTO invites (id,nom,prenom,responsable,activite) VALUES (%s,%s,%s,%s,%s);""", [inv[i]['id'],inv[i]['nom'],inv[i]['prenom'],inv[i]['responsable'],inv[i]['activite'],])
        if DEBUG:
            print [i,length]
    cur_new.execute("""COMMIT;""")

def import_rights():

    con_old, cur_old = getcursor(db_nk12)
    con_new, cur_new = getcursor(db_nk15)
    cur_old.execute("SELECT numcbde,prenom,nom,pseudo,droits,surdroits,supreme FROM adherents WHERE numcbde>-1"
                +" AND NOT droits in (0,1) AND supreme='f' ORDER BY droits;")
    l=cur_old.fetchall()

    #: Correspondance binaire → droit
    dicodroits={
            1 : "Intranet",
            2 : "Note",
            4 : "Consos",
            8 : "Adhérents",
           16 : "Boutons",
           32 : "Inscriptions",
           64 : "Activités",
          128 : "Invités",
          256 : "Transactions",
          512 : "Forcé",
         1024 : "Chèques",
         2048 : "Webmaster",
         4096 : "Wei",
         8192 : "Sendmail",
        16384 : "Bureau",
        32768 : "Accès extérieur",
        65536 : "Préinscriptions"
        }

    pow2=[2**i for i in range(17)]
    vrail=[]
    for ligne in l:
        droits = ",".join([dicodroits[i] for i in pow2 if ligne["droits"]&i])
        surdroits = ",".join([dicodroits[i] for i in pow2 if ligne["surdroits"]&i])
        print ligne['numcbde']
        print ligne['pseudo']
        print droits
        print surdroits
        new_droits = [u'login',u'basic']
        if 'Note' in droits:
            new_droits.append(u'note')
        if 'Forcé' in droits:
            new_droits.append(u'forced')
        if 'Adhérents' in droits:
            new_droits.append(u'compte')
        print ",".join(new_droits)
        cur_new.execute( """UPDATE comptes SET droits = %s WHERE idbde = %s""",[",".join(new_droits),ligne['numcbde']])
        if DEBUG:
            print [i,length]
    cur_new.execute("""COMMIT;""")

if __name__ == "__main__":
    if "-v" in sys.argv or "--debug" in sys.argv or "--verbose" in sys.argv:
        DEBUG = True
    if "--cmp" in sys.argv:
		import_adherents()
    if "--adh" in sys.argv:
		import_adhesions()
    if "--trans" in sys.argv:
		import_transactions()
    if "--act" in sys.argv:
		import_activites()
    if "--chq" in sys.argv:
		import_cheques()
    if "--bout" in sys.argv:
		import_boutons()
    if "--inv" in sys.argv:
		import_invite()
    if "--upd" in sys.argv:
		update_indexes()
    if "--rig" in sys.argv:
		import_rights()
    if "-A" in sys.argv:
		import_adherents()
		import_adhesions()
		import_transactions()
		import_activites()
		import_cheques()
		import_boutons()
		import_invite()
		update_indexes()


