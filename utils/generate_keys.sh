#!/bin/bash

# Ce script permet de générer une clé privée et un certificat autosigné pour
# le serveur de la note

### Paramètres du script

BASEDIR="/home/note/note-kfet-2015-serveur"
KEYSDIR="$BASEDIR/keys"

PKEYFILE="$KEYSDIR/note-kfet-2015-serveur.key"
CERTFILE="$KEYSDIR/note-kfet-2015-serveur.crt"

CSRFILE="$KEYSDIR/req.csr"

### Analyse de la ligne de commande

# Quel est l'utilisateur faisant tourner la note ?
case "$1" in
    "-u" | "--user") NOTEUSER="$2";;
    "-h") echo "Usage: $0 [-u|--user note_user]";
        exit 0;;
    *) NOTEUSER="note";;
esac

### Prérequis

# Le script doit être exécuté par un administrateur système pour pouvoir
# créer les dossiers nécessaires, générer les clés et donner le tout à $NOTEUSER
if [[ $(whoami) != "root" ]] ; then
    echo "Ce script doit être exécuté en tant que root."
    exit 1;
fi

# OpenSSL est-il installé sur la machine ?
if ! [ -x /usr/bin/openssl ] ; then
    echo "OpenSSL est requis pour générer le certificat."
    echo -n "Voulez-vous installer OpenSSL ? [o/N]"
    read INSTALL_OPENSSL
    case $INSTALL_OPENSSL in
        "o" | "O") apt-get install openssl;;
        *) exit 1;;
    esac;
fi

# L'utilisteur spécifié existe-t-il ?
if id -u $NOTEUSER >/dev/null 2>&1 ; then
    echo -n "Le certificat sera généré pour l'utilisateur $NOTEUSER. Continuer ? [o/N]"
    read CONT
    case $CONT in
        "o" | "O") ;;
        *) exit 1;;
    esac
else
    echo "L'utilisateur $NOTEUSER n'existe pas. Impossible de créer le certificat pour la note"
    exit 1;
fi

### Création du certificat

# Création du dossier des clés
echo -n "Création du dossier de clés ($KEYSDIR) ..."
mkdir -p $KEYSDIR
echo -e " Fait !\n"

# Génération de la clé privée
echo -e "Génération d'une clé privée pour le serveur de la note ...\n"
umask 077
openssl genpkey -algorithm RSA -out $PKEYFILE -pkeyopt rsa_keygen_bits:4096
echo -e "Clé générée\n"

# Génération du CSR
echo -e "Génération d'une demande de certificat ...\n"
umask 022
openssl req -new -key $PKEYFILE -out $CSRFILE
echo -e "Demande générée\n"

# Signature du certificat
echo -en "Création du certificat autosigné ..."
openssl x509 -req -in $CSRFILE -signkey $PKEYFILE -out $CERTFILE >/dev/null 2>&1
echo -e "Certificat créé !\n"

# Suppression des fichiers inutiles
echo -e "Nettoyage ...\n\n"
chown -R $NOTEUSER: $KEYSDIR
rm -f $CSRFILE

echo -e "Le certificat a été généré.\n\t Certificat: $CERTFILE \n\t Clé associée: $PKEYFILE"
