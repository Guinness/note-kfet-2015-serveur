#!/bin/bash

# À appeler tous les jours par cron
# lance le check de consistency et
# la mise à jour de l'historique

cd /home/note/note-kfet-2015-serveur/serveur/
./Consistency.py --expire-historique
./Consistency.py --check-consistency --mail
