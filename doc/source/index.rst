.. Note Kfet 2015 documentation master file, created by
   sphinx-quickstart on Tue Jan  8 22:34:25 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Documentation de la Note Kfet 2015
==================================

Table des matières :

.. toctree::
   :maxdepth: 2

   serveur/index
   config/index
   mail/index

   others

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

